<?php

if(isset($_POST['a']) && isset($_POST['b'])){

$a = $_POST['a'];
$b = $_POST['b'];

function getGcd($var1, $var2) {
  if (!$var2) {
    return $var1;
  }
  return getGcd($var2, $var1 % $var2);
  
 }

 $result = getGcd($a,$b);

 header("Location:index.php?r=$result" );
}
