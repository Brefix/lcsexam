<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Produit.php</title>
    <link rel="stylesheet" href="bootstrap.min.css">

</head>
<style>
        .padding{
            padding-top: 90px;
        }
    </style>
<body class="padding">
<header>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
            <div class="container-fluid">
                <a href="" class="navbar-brand " style="margin: auto;">GCD CALCULATOR</a>
            </div>
        </nav>
    </header>

<main>
        
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-3 "></div>
            <div class="col-lg-6">
            

            <form action="result.php" method="post" id="form_addclub">
                <div class="row">
                    <div class="col my-4">
                        <label for="ref" class="form-label fw-semibold">A</label>
                        <input type="text" class="form-control" id="a" name="a">
                    </div>
    
                    <div class="col my-4">
                        <label for="libel" class="form-label fw-semibold">B</label>
                        <input type="text" class="form-control" id="b" name="b">
                    </div>

                </div>
                

            
                <div class="row">
                    <div class="col"></div>
                    <div class="col my-4">
                        <input type="submit" id="go" value="GO" class="form-control text-white bg-danger">
                    </div>
                    <div class="col"></div>
                </div>
                <div class="row">
                    <div class="col"></div>
                    <div class="col my-2">
                    <?php
                        if(isset($_GET['r'])){
                        $r = $_GET['r'];
                        echo "<input type=button id=result value= $r class='form-control text-white bg-dark' disabled>";
                    }

                    ?>
                    </div>
                    <div class="col"></div>
                </div>

                
            </form>

            </div>
        
        
        </div>
    </div>

</main>
</body>

</html>