var day = document.getElementById('day');
var month = document.getElementById('month');
var year = document.getElementById('year');
var go = document.getElementById('go');
var result = document.getElementById('result');
go.addEventListener('click', function(){

    result.value = checker(parseInt(day.value,10),parseInt(month.value,10),parseInt(year.value,10))?'V A L I D !':'invalid';
    
});

function checker(d,m,y) {
    //logic: on verifie si le mois est valide ensuite on pass au jour dont la validite depend du type d'annee

    let days = [31,29,30,31,30,31,30,31,30,31,30,31];

    // entrees doivent etre superieur a 0
    [d,m,y].map(e=>{
        if(e<=0) {return false}
    })
    
    //mois ne doit pas etre superieur a 12
    if( m > 12  ){
        return false
    }

    // jour .... 
    for (let i = 0; i < 12; i++) {
        
        if (m == i+1 && days[i]>=d) {
            if ( i == 1 && y%4 != 0 && d >= 29) {
                return false
            }
            return true
        }
        
    }
    return false

}
